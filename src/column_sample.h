#ifndef COLUMN_SAMPLE_H
#define COLUMN_SAMPLE_H

#include "glam2.h"

void column_sample(glam2_aln *aln, data *d, const double temperature);

#endif
