#ifndef DNA_PRIOR_H
#define DNA_PRIOR_H

#include "dirichlet.h"

void dmix_dna(dirichlet_mix *m);

#endif
