#ifndef SCAN_OUTPUT_H
#define SCAN_OUTPUT_H

#include <stdio.h>
#include "scan.h"

void print_hits(FILE *fp, const alignment *alns, const data *d);

#endif
