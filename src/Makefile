# This file holds commands for compiling programs in the GLAM2 package

# These commands are customized for the gcc compiler, and may need to
# be modified for other compilers

# -Wall means turn on compiler warnings (optional)
# -O3 means optimization level 3 (the maximum for gcc)
# -lm means include the math library

# Source files for glam2:
GSRC = alphabet.c args.c column_sample.c dirichlet.c dna_prior.c	\
fasta.c glam2.c glam2_aln.c init.c output.c recode3_20comp.c		\
site_sample.c util.c

# Source files for glam2scan:
SSRC = alphabet.c scan_args.c dirichlet.c dna_prior.c fasta.c scan.c	\
scan_init.c scan_output.c recode3_20comp.c util.c heap.c motif.c	\
alignment.c

# Source files for glam2format:
FSRC = glam2format.c alignment.c fasta.c util.c

# Source files for glam2mask:
MSRC = glam2mask.c alignment.c fasta.c util.c

# Compiler options:
CFLAGS = -Wall -O3

# Default action: compile glam2, glam2scan, glam2format, and glam2mask
all: glam2 glam2scan glam2format glam2mask

# Command for compiling glam2:
glam2: $(GSRC) *.h Makefile
	cc $(CFLAGS) -o glam2 $(GSRC) -lm

# Command for compiling glam2scan:
glam2scan: $(SSRC) *.h Makefile
	cc $(CFLAGS) -o glam2scan $(SSRC) -lm

# Command for compiling glam2format:
glam2format: $(FSRC) *.h Makefile
	cc $(CFLAGS) -o glam2format $(FSRC) -lm

# Command for compiling glam2mask:
glam2mask: $(MSRC) *.h Makefile
	cc $(CFLAGS) -o glam2mask $(MSRC) -lm

# Here follow commands for compiling special versions of the programs

# Compile glam2 including FFT algorithm (requires FFTW to be installed):
glam2fft: $(GSRC) convolve.c *.h Makefile
	cc $(CFLAGS) -DFFT -o glam2fft $(GSRC) convolve.c -lm -lfftw3

# Compile for debugging with gdb or valgrind, with extra compiler warnings:
glam2_d: $(GSRC) *.h Makefile
	cc -Wall -W -pedantic -g -o glam2_d $(GSRC) -lm
glam2scan_d: $(SSRC) *.h Makefile
	cc -Wall -W -pedantic -g -o glam2scan_d $(SSRC) -lm
glam2format_d: $(FSRC) *.h Makefile
	cc -Wall -W -pedantic -g -o glam2format_d $(FSRC) -lm
glam2mask_d: $(MSRC) *.h Makefile
	cc -Wall -W -pedantic -g -o glam2mask_d $(MSRC) -lm

# Compile for profiling with gprof, with extra compiler warnings:
# Use -O2 to avoid function inlining
glam2_p: $(GSRC) *.h Makefile
	cc -Wall -W -pedantic -O2 -pg -o glam2_p $(GSRC) -lm
glam2scan_p: $(SSRC) *.h Makefile
	cc -Wall -W -pedantic -O2 -pg -o glam2scan_p $(SSRC) -lm
